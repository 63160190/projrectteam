/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ReportIngredient {
    String ingredient_name;
    int ingredient_amount_in;
    int ingredient_amount_out;
    int ingredient_total;

    public ReportIngredient(String ingredient_name, int ingredient_amount_in, int ingredient_amount_out, int ingredient_total) {
        this.ingredient_name = ingredient_name;
        this.ingredient_amount_in = ingredient_amount_in;
        this.ingredient_amount_out = ingredient_amount_out;
        this.ingredient_total = ingredient_total;
    }

    private ReportIngredient() {
        
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    public int getIngredient_amount_in() {
        return ingredient_amount_in;
    }

    public void setIngredient_amount_in(int ingredient_amount_in) {
        this.ingredient_amount_in = ingredient_amount_in;
    }

    public int getIngredient_amount_out() {
        return ingredient_amount_out;
    }

    public void setIngredient_amount_out(int ingredient_amount_out) {
        this.ingredient_amount_out = ingredient_amount_out;
    }

    public int getIngredient_total() {
        return ingredient_total;
    }

    public void setIngredient_total(int ingredient_total) {
        this.ingredient_total = ingredient_total;
    }

    @Override
    public String toString() {
        return "ReportIngredient{" + "ingredient_name=" + ingredient_name + ", ingredient_amount_in=" + ingredient_amount_in + ", ingredient_amount_out=" + ingredient_amount_out + ", ingredient_total=" + ingredient_total + '}';
    }
    
    public static ReportIngredient fromRS(ResultSet rs) {
                   ReportIngredient obj = new ReportIngredient();
        try {
            
            obj.setIngredient_name(rs.getString("ingredient_name"));
            obj.setIngredient_amount_in(rs.getInt("ingredient_amount_in"));
            obj.setIngredient_amount_out(rs.getInt("ingredient_amount_out"));
            obj.setIngredient_total(rs.getInt("ingredient_total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportIngredient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
