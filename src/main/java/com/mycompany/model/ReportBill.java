/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ReportBill {
    String employee_id;
    double bill_amount_purc;
    double bill_total;
    String period; //time

    public ReportBill(String employee_id, double bill_amount_purc, double bill_total, String period) {
        this.employee_id = employee_id;
        this.bill_amount_purc = bill_amount_purc;
        this.bill_total = bill_total;
        this.period = period;
    }

    private ReportBill() {
       
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public double getBill_amount_purc() {
        return bill_amount_purc;
    }

    public void setBill_amount_purc(double bill_amount_purc) {
        this.bill_amount_purc = bill_amount_purc;
    }

    public double getBill_total() {
        return bill_total;
    }

    public void setBill_total(double bill_total) {
        this.bill_total = bill_total;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "ReportBill{" + "employee_id=" + employee_id + ", bill_amount_purc=" + bill_amount_purc + ", bill_total=" + bill_total + ", bill_date=" + period + '}';
    }

    
    public static ReportBill fromRS(ResultSet rs) {
                    ReportBill obj = new ReportBill();
        try {
            obj.setEmployee_id(rs.getString("employee_id"));
            obj.setBill_amount_purc(rs.getDouble("bill_amount_purc"));
            obj.setBill_total(rs.getDouble("bill_total"));
            obj.setPeriod(rs.getString("period"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportBill.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}
