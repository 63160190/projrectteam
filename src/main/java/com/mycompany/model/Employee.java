/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Employee {

    private int employee_id;
    private int salary_id;
    private int work_time_id;
    private String employee_name;
    private String employee_surname;
    private String employee_tel;
    private int employee_jobtitle;
    private String employee_username;
    private String employee_password;

    public Employee(int employee_id, int salary_id, int work_time_id, String employee_name, String employee_surname, String employee_tel, int employee_jobtitle, String employee_username, String employee_password) {
        this.employee_id = employee_id;
        this.salary_id = salary_id;
        this.work_time_id = work_time_id;
        this.employee_name = employee_name;
        this.employee_surname = employee_surname;
        this.employee_tel = employee_tel;
        this.employee_jobtitle = employee_jobtitle;
        this.employee_username = employee_username;
        this.employee_password = employee_password;
    }

    public Employee() {
        this.employee_id = -1;
        this.employee_name = "";
        this.employee_surname = "";
        this.employee_tel = "";
        this.employee_jobtitle = 0;
        this.employee_username = "";
        this.employee_password = "";
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public int getSalary_id() {
        return salary_id;
    }

    public void setSalary_id(int salary_id) {
        this.salary_id = salary_id;
    }

    public int getWork_time_id() {
        return work_time_id;
    }

    public void setWork_time_id(int work_time_id) {
        this.work_time_id = work_time_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_surname() {
        return employee_surname;
    }

    public void setEmployee_surname(String employee_surname) {
        this.employee_surname = employee_surname;
    }

    public String getEmployee_tel() {
        return employee_tel;
    }

    public void setEmployee_tel(String employee_tel) {
        this.employee_tel = employee_tel;
    }

    public int getEmployee_jobtitle() {
        return employee_jobtitle;
    }

    public void setEmployee_jobtitle(int employee_jobtitle) {
        this.employee_jobtitle = employee_jobtitle;
    }

    public String getEmployee_username() {
        return employee_username;
    }

    public void setEmployee_username(String employee_username) {
        this.employee_username = employee_username;
    }

    public String getEmployee_password() {
        return employee_password;
    }

    public void setEmployee_password(String employee_password) {
        this.employee_password = employee_password;
    }

    @Override
    public String toString() {
        return "Employee{" + "employee_id=" + employee_id + ", salary_id=" + salary_id + ", work_time_id=" + work_time_id + ", employee_name=" + employee_name + ", employee_surname=" + employee_surname + ", employee_tel=" + employee_tel + ", employee_jobtitle=" + employee_jobtitle + ", employee_username=" + employee_username + ", employee_password=" + employee_password + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setEmployee_id(rs.getInt("employee_id"));
//            employee.setSalary_id(rs.getInt("salary_id"));
            employee.setWork_time_id(rs.getInt("work_time_id"));
            employee.setEmployee_name(rs.getString("employee_name"));
            employee.setEmployee_surname(rs.getString("employee_surname"));
            employee.setEmployee_tel(rs.getString("employee_tel"));
            employee.setEmployee_jobtitle(rs.getInt("employee_jobtitle"));
            employee.setEmployee_username(rs.getString("employee_username"));
            employee.setEmployee_password(rs.getString("employee_password"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
