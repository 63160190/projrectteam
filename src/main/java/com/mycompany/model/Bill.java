/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Bill {

    private int bill_id;
    private int buy_id;
    private int employee_id;
    private Double bill_amount;
    private Double bill_total;
    private Double bill_cash;
    private Double bill_change;
    private Date bill_date;

    public Bill(int bill_id, int buy_id, int employee_id, Double bill_amount, Double bill_total, Double bill_cash, Double bill_change, Date bill_date) {
        this.bill_id = bill_id;
        this.buy_id = buy_id;
        this.employee_id = employee_id;
        this.bill_amount = bill_amount;
        this.bill_total = bill_total;
        this.bill_cash = bill_cash;
        this.bill_change = bill_change;
        this.bill_date = bill_date;
    }

    public Bill() {
        this.bill_id =-1;
        this.buy_id = -1;
        this.employee_id = -1;
        this.bill_amount =0.0;
        this.bill_total =0.0;
        this.bill_cash = 0.0;
        this.bill_change =0.0;
        this.bill_date = new Date();
    }

    public int getBill_id() {
        return bill_id;
    }

    public int getBuy_id() {
        return buy_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public Double getBill_amount() {
        return bill_amount;
    }

    public Double getBill_total() {
        return bill_total;
    }

    public Double getBill_cash() {
        return bill_cash;
    }

    public Double getBill_change() {
        return bill_change;
    }

    public Date getBill_date() {
        return bill_date;
    }

    public void setBill_id(int bill_id) {
        this.bill_id = bill_id;
    }

    public void setBuy_id(int buy_id) {
        this.buy_id = buy_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setBill_amount(Double bill_amount) {
        this.bill_amount = bill_amount;
    }

    public void setBill_total(Double bill_total) {
        this.bill_total = bill_total;
    }

    public void setBill_cash(Double bill_cash) {
        this.bill_cash = bill_cash;
    }

    public void setBill_change(Double bill_change) {
        this.bill_change = bill_change;
    }

    public void setBill_date(Date bill_date) {
        this.bill_date = bill_date;
    }

    @Override
    public String toString() {
        return "Bill{" + "bill_id=" + bill_id + ", buy_id=" + buy_id + ", employee_id=" + employee_id + ", bill_amount=" + bill_amount + ", bill_total=" + bill_total + ", bill_cash=" + bill_cash + ", bill_change=" + bill_change + ", bill_date=" + bill_date + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setBill_id(rs.getInt("bill_id"));
            bill.setBuy_id(rs.getInt("buy_id"));
            bill.setEmployee_id(rs.getInt("employee_id"));
            bill.setBill_amount(rs.getDouble("bill_amount_purc"));
            bill.setBill_total(rs.getDouble("bill_total"));
            bill.setBill_cash(rs.getDouble("bill_cash"));
            bill.setBill_change(rs.getDouble("bill_change"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            bill.setBill_date(sdf.parse(rs.getString("bill_date")));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bill;
    }

}
