/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Buy {

    private int buy_id;
    private int report_id;
    private int empolyee_id;
    private Date buy_date;

    public Buy(int buy_id, int report_id, int empolyee_id, Date buy_date) {
        this.buy_id = buy_id;
        this.report_id = report_id;
        this.empolyee_id = empolyee_id;
        this.buy_date = buy_date;
    }

    public Buy() {
        this.buy_id = -1;
        this.report_id = -1;
        this.empolyee_id = -1;
        this.buy_date = new Date();
    }

    public int getBuy_id() {
        return buy_id;
    }

    public void setBuy_id(int buy_id) {
        this.buy_id = buy_id;
    }

    public int getReport_id() {
        return report_id;
    }

    public void setReport_id(int report_id) {
        this.report_id = report_id;
    }

    public int getEmpolyee_id() {
        return empolyee_id;
    }

    public void setEmpolyee_id(int empolyee_id) {
        this.empolyee_id = empolyee_id;
    }

    public Date getBuy_date() {
        return buy_date;
    }

    public void setBuy_date(Date buy_date) {
        this.buy_date = buy_date;
    }

    @Override
    public String toString() {
        return "Buy{" + "buy_id=" + buy_id + ", report_id=" + report_id + ", empolyee_id=" + empolyee_id + ", buy_date=" + buy_date + '}';
    }

    public static Buy fromRS(ResultSet rs) {
        Buy buy = new Buy();
        try {
            buy.setBuy_id(rs.getInt("buy_id"));
            buy.setReport_id(rs.getInt("report_id"));
            buy.setEmpolyee_id(rs.getInt("empolyee_id"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            buy.setBuy_date(sdf.parse(rs.getString("buy_date")));
        } catch (SQLException ex) {
            Logger.getLogger(Buy.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Buy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return buy;
    }

}
