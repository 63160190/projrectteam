/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oil
 */
public class Order_item {

    private int order_item_id;
    private int product_id;
    private int order_item_amount;

    public Order_item(int order_item_id, int product_id, int order_item_amount) {
        this.order_item_id = order_item_id;
        this.product_id = product_id;
        this.order_item_amount = order_item_amount;
    }

    public Order_item(int product_id, int order_item_amount) {
        this.order_item_id = -1;
        this.product_id = product_id;
        this.order_item_amount = order_item_amount;
    }

    public int getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(int order_item_id) {
        this.order_item_id = order_item_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getOrder_item_amount() {
        return order_item_amount;
    }

    public void setOrder_item_amount(int order_item_amount) {
        this.order_item_amount = order_item_amount;
    }

 
    public Order_item() {
        this.order_item_id = -1;
        this.product_id = -1;
        this.order_item_amount = 0;
    }

    @Override
    public String toString() {
        return "Order_item{" + "order_item_id=" + order_item_id + ", product_id=" + product_id + ", order_item_id_amount=" + order_item_amount + '}';
    }

    

    public static Order_item fromRS(ResultSet rs) {
        Order_item order_item = new Order_item();
        try {
            order_item.setOrder_item_id(rs.getInt("order_item_id"));
            order_item.setProduct_id(rs.getInt("product_id"));
            order_item.setOrder_item_amount(rs.getInt("order_item_amount"));
        } catch (SQLException ex) {
            Logger.getLogger(Order_item.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return order_item;
    }
}
