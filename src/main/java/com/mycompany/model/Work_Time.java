/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Work_Time {

    private int work_time_id;
    private Date work_time_start_time;
    private Date work_time_end_time;
    private Date work_time_sum_worktime;

    public Work_Time(int work_time_id, Date work_time_start_time, Date work_time_end_time, Date work_time_sum_worktime) {
        this.work_time_id = work_time_id;
        this.work_time_start_time = work_time_start_time;
        this.work_time_end_time = work_time_end_time;
        this.work_time_sum_worktime = work_time_sum_worktime;
    }

    public Work_Time() {
        this.work_time_id = -1;
        this.work_time_start_time = new Date();
        this.work_time_end_time = new Date();
        this.work_time_sum_worktime = new Date();
    }

    public Work_Time(int work_time_id) {
        this.work_time_id = work_time_id;
        this.work_time_start_time = new Date();
        this.work_time_end_time = new Date();
        this.work_time_sum_worktime = new Date();
        
    }

    public int getWork_time_id() {
        return work_time_id;
    }

    public Date getWork_time_start_time() {
        return work_time_start_time;
    }

    public Date getWork_time_end_time() {
        return work_time_end_time;
    }

    public Date getWork_time_sum_worktime() {
        return work_time_sum_worktime;
    }

    public void setWork_time_id(int work_time_id) {
        this.work_time_id = work_time_id;
    }

    public void setWork_time_start_time(Date work_time_start_time) {
        this.work_time_start_time = work_time_start_time;
    }

    public void setWork_time_end_time(Date work_time_end_time) {
        this.work_time_end_time = work_time_end_time;
    }

    public void setWork_time_sum_worktime(Date work_time_sum_worktime) {
        this.work_time_sum_worktime = work_time_sum_worktime;
    }

    @Override
    public String toString() {
        return "Work_Time{" + "work_time_id=" + work_time_id + ", work_time_start_time=" + work_time_start_time + ", work_time_end_time=" + work_time_end_time + ", work_time_sum_worktime=" + work_time_sum_worktime + '}';
    }

    public static Work_Time fromRS(ResultSet rs) {
        Work_Time work_Time = new Work_Time();
        try {
            work_Time.setWork_time_id(rs.getInt("work_time_id"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            work_Time.setWork_time_start_time((java.sql.Date) sdf.parse(rs.getString("work_time_start_time")));
            work_Time.setWork_time_end_time((java.sql.Date) sdf.parse(rs.getString("work_time_end_time")));
            work_Time.setWork_time_sum_worktime((java.sql.Date) sdf.parse(rs.getString("work_time_sum_worktime")));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
        return work_Time;
    }

    public static Work_Time fromRS1(ResultSet rs) {
        Work_Time work_Time = new Work_Time();
        try {
            work_Time.setWork_time_id(rs.getInt("work_time_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return work_Time;
    }

}
