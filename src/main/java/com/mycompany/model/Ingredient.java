/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Ingredient {

    private int ingredient_id;
    private String ingredient_name;
    private int ingredient_amount_in;
    private int ingredient_amount_out;
    private int ingredient_total;
    private int ingredient_min_amount;
    private String ingredient_unit;

    public Ingredient(int ingredient_id, String ingredient_name, int ingredient_amount_out, int ingredient_total, int ingredient_amount_in, int ingredient_min_amount, String ingredient_unit) {
        this.ingredient_id = ingredient_id;
        this.ingredient_name = ingredient_name;
        this.ingredient_amount_in = ingredient_amount_in;
        this.ingredient_amount_out = ingredient_amount_out;
        this.ingredient_min_amount = ingredient_min_amount;
        this.ingredient_total = ingredient_total;
        this.ingredient_unit = ingredient_unit;
    }

    public Ingredient(String ingredient_name, int ingredient_amount_out, int ingredient_total, int ingredient_amount_in, int ingredient_min_amount, String ingredient_unit) {
        this.ingredient_id = -1;
        this.ingredient_name = ingredient_name;
        this.ingredient_unit = ingredient_unit;
        this.ingredient_total = ingredient_total;
        this.ingredient_amount_in = ingredient_amount_in;
        this.ingredient_amount_out = ingredient_amount_out;
        this.ingredient_min_amount = ingredient_min_amount;

    }

    public Ingredient() {
        this.ingredient_id = -1;
        this.ingredient_name = "";
        this.ingredient_unit = "";
        this.ingredient_total = 0;
        this.ingredient_amount_in = 0;
        this.ingredient_amount_out = 0;
        this.ingredient_min_amount = 0;
    }

    public int getIngredient_id() {
        return ingredient_id;
    }

    public void setIngredient_id(int ingredient_id) {
        this.ingredient_id = ingredient_id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    public int getIngredient_amount_in() {
        return ingredient_amount_in;
    }

    public void setIngredient_amount_in(int ingredient_amount_in) {
        this.ingredient_amount_in = ingredient_amount_in;
    }

    public int getIngredient_amount_out() {
        return ingredient_amount_out;
    }

    public void setIngredient_amount_out(int ingredient_amount_out) {
        this.ingredient_amount_out = ingredient_amount_out;
    }

    public int getIngredient_total() {
        return ingredient_total;
    }

    public void setIngredient_total(int ingredient_total) {
        this.ingredient_total = ingredient_total;
    }

    public int getIngredient_min_amount() {
        return ingredient_min_amount;
    }

    public void setIngredient_min_amount(int ingredient_min_amount) {
        this.ingredient_min_amount = ingredient_min_amount;
    }

    public String getIngredient_unit() {
        return ingredient_unit;
    }

    public void setIngredient_unit(String ingredient_unit) {
        this.ingredient_unit = ingredient_unit;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "ingredient=" + ingredient_id + ", ingredient_name=" + ingredient_name + ", ingredient_amount_in=" + ingredient_amount_in + ", ingredient_amount_out=" + ingredient_amount_out + "ingredient_amount_in=" + ingredient_amount_in + "ingredient_min_amount=" + ingredient_min_amount + ", ingredient_unit=" + ingredient_unit + '}';
    }

    public static Ingredient fromRS(ResultSet rs) {
        Ingredient ing = new Ingredient();
        try {
            ing.setIngredient_id(rs.getInt("ingredient_id"));
            ing.setIngredient_name(rs.getString("ingredient_name"));
            ing.setIngredient_amount_in(rs.getInt("ingredient_amount_in"));
            ing.setIngredient_amount_out(rs.getInt("ingredient_amount_out"));
            ing.setIngredient_total(rs.getInt("ingredient_total"));
            ing.setIngredient_min_amount(rs.getInt("ingredient_min_amount"));
            ing.setIngredient_unit(rs.getString("ingredient_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(Ingredient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ing;
    }
}
