/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oil
 */
public class Salary {
    private int salary_id;
    private int work_time_id;
    private Date salary_start_pay;
    private Date salary_end_pay;
    private double salary_decrease;
    private double salary_sum;

    

    public Salary(int salary_id, int work_time_id, Date salary_start_pay, Date salary_end_pay, double salary_decrease, double salaly_sum) {
        this.salary_id = salary_id;
        this.work_time_id = work_time_id;
        this.salary_start_pay = salary_start_pay;
        this.salary_end_pay = salary_end_pay;
        this.salary_decrease = salary_decrease;
        this.salary_sum = salaly_sum;
    }

    public Salary() {
        this.salary_id = -1;
        this.work_time_id = -1;
        this.salary_start_pay = new Date();
        this.salary_end_pay = new Date();
        this.salary_decrease = 0.0;
        this.salary_sum = 0.0;
    }

    @Override
    public String toString() {
        return "Salaly{" + "salary_id=" + salary_id + ", work_time_id=" + work_time_id + ", salary_start_pay=" + salary_start_pay + ", salary_end_pay=" + salary_end_pay + ", salary_decrease=" + salary_decrease + ", salaly_sum=" + salary_sum + '}';
    }

    public void setSalary_id(int salary_id) {
        this.salary_id = salary_id;
    }

    public void setWork_time_id(int work_time_id) {
        this.work_time_id = work_time_id;
    }

    public void setSalary_start_pay(Date salary_start_pay) {
        this.salary_start_pay = salary_start_pay;
    }

    public void setSalary_end_pay(Date salary_end_pay) {
        this.salary_end_pay = salary_end_pay;
    }

    public void setSalary_decrease(double salary_decrease) {
        this.salary_decrease = salary_decrease;
    }
    public void setSalary_sum(double salary_sum) {
        this.salary_sum = salary_sum;
    }

    public int getSalary_id() {
        return salary_id;
    }

    public int getWork_time_id() {
        return work_time_id;
    }

    public Date getSalary_start_pay() {
        return salary_start_pay;
    }

    public Date getSalary_end_pay() {
        return salary_end_pay;
    }

    public double getSalary_decrease() {
        return salary_decrease;
    }

    public double getSalaly_sum() {
        return salary_sum;
    }
    public static Salary fromRS(ResultSet rs) {
        Salary salaly = new Salary();
        try {
            salaly.setSalary_id(rs.getInt("salary_id"));
            salaly.setWork_time_id(rs.getInt("buy_id"));
            salaly.setSalary_decrease(rs.getDouble("salary_decrease"));
            salaly.setSalary_sum(rs.getDouble("salaly_sum"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            salaly.setSalary_start_pay((java.sql.Date) sdf.parse(rs.getString("salary_start_pay")));
            salaly.setSalary_end_pay((java.sql.Date) sdf.parse(rs.getString("salary_end_pay")));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salaly;
    }

}


      