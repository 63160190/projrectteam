/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sairung
 */
public class User {

    private int user_id;
    private int employee_id;
    private String user_username;
    private String user_password;

    public User(int user_id, int employee_id, String user_username, String user_password) {
        this.user_id = user_id;
        this.employee_id = employee_id;
        this.user_username = user_username;
        this.user_password = user_password;
    }

    public User() {
        this.user_id = -1;
        this.employee_id = -1;
        this.user_username = "";
        this.user_password = "";
    }

    public int getUser_id() {
        return user_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public String getUser_username() {
        return user_username;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    @Override
    public String toString() {
        return "User{" + "user_id=" + user_id + ", employee_id=" + employee_id + ", user_username=" + user_username + ", user_password=" + user_password + '}';
    }

    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setUser_id(rs.getInt("user_id"));
            user.setEmployee_id(rs.getInt("employee_id"));
            user.setUser_username(rs.getString("User_username"));
            user.setUser_password(rs.getString("user_password"));
        } catch (SQLException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }
}
