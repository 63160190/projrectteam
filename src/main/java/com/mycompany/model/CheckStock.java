/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

/**
 *
 * @author User
 */
public class CheckStock {

    private int check_stock_id;
    private int ingredient_id;
    private int employee_id;
    private Date check_stock_date;

    public CheckStock(int check_stock_id, int ingredient_id, int employee_id, Date check_stock_date) {
        this.check_stock_id = check_stock_id;
        this.ingredient_id = ingredient_id;
        this.employee_id = employee_id;
        this.check_stock_date = check_stock_date;
    }

    public CheckStock() {
        this.check_stock_id = -1;
        this.ingredient_id = -1;
        this.employee_id = -1;
        this.check_stock_date = new Date();
    }

    public int getCheck_stock_id() {
        return check_stock_id;
    }

    public int getIngredient_id() {
        return ingredient_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public Date getCheck_stock_date() {
        return check_stock_date;
    }

    public void setCheck_stock_id(int check_stock_id) {
        this.check_stock_id = check_stock_id;
    }

    public void setIngredient_id(int ingredient_id) {
        this.ingredient_id = ingredient_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setCheck_stock_date(Date check_stock_date) {
        this.check_stock_date = check_stock_date;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "check_stock_id=" + check_stock_id + ", ingredient_id=" + ingredient_id + ", employee_id=" + employee_id + ", check_stock_date=" + check_stock_date + '}';
    }

    public static CheckStock fromRS(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setCheck_stock_id(rs.getInt("check_stock_id"));
            checkStock.setIngredient_id(rs.getInt("ingredient_id"));
            checkStock.setEmployee_id(rs.getInt("empolyee_id"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            checkStock.setCheck_stock_date(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkStock;
    }

}
