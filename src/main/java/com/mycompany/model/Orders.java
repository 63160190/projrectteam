/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Orders {

    private int order_id;
    private int order_item_id;
    private int employee_id;
    private int customer_id;
    private int order_queue;
    private double order_total;
    private double order_total_net;
    private double order_cash;
    private double order_change;
    private double order_discount;
    private Date order_datetime;
    private int order_qty;

    public Orders() {
        this.order_id = -1;
        this.order_id = -1;
        this.order_item_id = -1;
        this.employee_id = -1;
        this.customer_id = -1;
        this.order_queue = 0;
        this.order_total = 0.00;
        this.order_total_net = 0.00;
        this.order_cash = 0.00;
        this.order_change = 0.00;
        this.order_discount = 0.00;
        this.order_datetime = new Date();
        this.order_qty = 0;
        
    }

    public Orders(int order_id, int order_item_id, int employee_id, int customer_id, int order_queue, double order_total, double order_total_net, double order_cash, double order_change, double order_discount, Date order_datetime, int order_qty) {
        this.order_id = order_id;
        this.order_item_id = order_item_id;
        this.employee_id = employee_id;
        this.customer_id = customer_id;
        this.order_queue = order_queue;
        this.order_total = order_total;
        this.order_total_net = order_total_net;
        this.order_cash = order_cash;
        this.order_change = order_change;
        this.order_discount = order_discount;
        this.order_datetime = order_datetime;
        this.order_qty = order_qty;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(int order_item_id) {
        this.order_item_id = order_item_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getOrder_queue() {
        return order_queue;
    }

    public void setOrder_queue(int order_queue) {
        this.order_queue = order_queue;
    }

    public double getOrder_total() {
        return order_total;
    }

    public void setOrder_total(double order_total) {
        this.order_total = order_total;
    }

    public double getOrder_total_net() {
        return order_total_net;
    }

    public void setOrder_total_net(double order_total_net) {
        this.order_total_net = order_total_net;
    }

    public double getOrder_cash() {
        return order_cash;
    }

    public void setOrder_cash(double order_cash) {
        this.order_cash = order_cash;
    }

    public double getOrder_change() {
        return order_change;
    }

    public void setOrder_change(double order_change) {
        this.order_change = order_change;
    }

    public double getOrder_discount() {
        return order_discount;
    }

    public void setOrder_discount(double order_discount) {
        this.order_discount = order_discount;
    }

    public Date getOrder_datetime() {
        return order_datetime;
    }

    public void setOrder_datetime(Date order_datetime) {
        this.order_datetime = order_datetime;
    }

    public int getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(int order_qty) {
        this.order_qty = order_qty;
    }

    @Override
    public String toString() {
        return "Orders{" + "order_id=" + order_id + ", order_item_id=" + order_item_id + ", employee_id=" + employee_id + ", customer_id=" + customer_id + ", order_queue=" + order_queue + ", order_total=" + order_total + ", order_total_net=" + order_total_net + ", order_cash=" + order_cash + ", order_change=" + order_change + ", order_discount=" + order_discount + ", order_datetime=" + order_datetime + ", order_qty=" + order_qty + '}';
    }
    
    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setOrder_id(rs.getInt("order_id"));
            order.setOrder_item_id(rs.getInt("order_item_id"));
            order.setEmployee_id(rs.getInt("employee_id"));
            order.setCustomer_id(rs.getInt("customer_id"));
            order.setOrder_queue(rs.getInt("order_queue"));
            order.setOrder_total(rs.getDouble("order_total"));
            order.setOrder_total_net(rs.getDouble("order_total_net"));
            order.setOrder_cash(rs.getDouble("order_cash"));
            order.setOrder_change(rs.getDouble("order_change"));
            order.setOrder_discount(rs.getDouble("order_discount"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setOrder_qty(rs.getInt("order_qty"));
            order.setOrder_datetime((java.sql.Date) sdf.parse(rs.getString("order_datetime")));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
}
