/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oil
 */
public class Report {

    private int report_id;
    private int check_stock_id;
    private int employee_id;
    private int report_want_buy;
    private double report_total;

    public Report(int report_id, int check_stock_id, int employee_id, int report_want_buy, double report_total) {
        this.report_id = report_id;
        this.check_stock_id = check_stock_id;
        this.employee_id = employee_id;
        this.report_want_buy = report_want_buy;
        this.report_total = report_total;
    }

    public Report() {
        this.report_id = -1;
        this.check_stock_id = -1;
        this.employee_id = -1;
        this.report_want_buy = 0;
        this.report_total = 0.0;
    }

    public int getReport_id() {
        return report_id;
    }

    public int getCheck_stock_id() {
        return check_stock_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public int getReport_want_buy() {
        return report_want_buy;
    }

    public double getReport_total() {
        return report_total;
    }

    public void setReport_id(int report_id) {
        this.report_id = report_id;
    }

    public void setCheck_stock_id(int check_stock_id) {
        this.check_stock_id = check_stock_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public void setReport_want_buy(int report_want_buy) {
        this.report_want_buy = report_want_buy;
    }

    public void setReport_total(double report_total) {
        this.report_total = report_total;
    }

    @Override
    public String toString() {
        return "Report{" + "report_id=" + report_id + ", check_stock_id=" + check_stock_id + ", employee_id=" + employee_id + ", report_want_buy=" + report_want_buy + ", report_total=" + report_total + '}';
    }

    public static Report fromRS(ResultSet rs) {
        Report report = new Report();
        try {
            report.setReport_id(rs.getInt("report_id"));
            report.setCheck_stock_id(rs.getInt("check_stock_id"));
            report.setEmployee_id(rs.getInt("employee_id"));
            report.setReport_want_buy(rs.getInt("report_want_buy"));
            report.setReport_total(rs.getDouble("report_total"));

        } catch (SQLException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return report;
    }
}
