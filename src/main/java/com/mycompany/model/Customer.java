/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Customer {
    private int customer_id;
    private String customer_name;
    private String customer_surname;
    private String customer_tel;
    private int customer_point;
    private int customer_amount;
    private int customer_discount;
    

    public Customer(int customer_id, String customer_name, String customer_surname, String customer_tel, int customer_point, int customer_amount, int customer_discount) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_surname = customer_surname;
        this.customer_tel = customer_tel;
        this.customer_point = customer_point;
        this.customer_amount = customer_amount;
        this.customer_discount = customer_discount;
    }

    public Customer() {
       this.customer_id = -1;
        this.customer_name = "";
        this.customer_surname = "";
        this.customer_tel = "";
        this.customer_point = 0;
        this.customer_amount = 0;
        this.customer_discount = 0;
    }
    
    public Customer(String customer_tel){
        this.customer_id = -1;
        this.customer_name = "";
        this.customer_surname = "";
        this.customer_tel = "";
        this.customer_tel = customer_tel;
        this.customer_amount = 0;
        this.customer_discount = 0;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_surname() {
        return customer_surname;
    }

    public void setCustomer_surname(String customer_surname) {
        this.customer_surname = customer_surname;
    }

    public String getCustomer_tel() {
        return customer_tel;
    }

    public void setCustomer_tel(String customer_tel) {
        this.customer_tel = customer_tel;
    }

    public int getCustomer_point() {
        return customer_point;
    }

    public void setCustomer_point(int customer_point) {
        this.customer_point = customer_point;
    }

    public int getCustomer_amount() {
        return customer_amount;
    }

    public void setCustomer_amount(int customer_amount) {
        this.customer_amount = customer_amount;
    }

    public int getCustomer_discount() {
        return customer_discount;
    }

    public void setCustomer_discount(int customer_discount) {
        this.customer_discount = customer_discount;
    }

    @Override
    public String toString() {
        return "Customer{" + "customer_id=" + customer_id + ", customer_name=" + customer_name + ", customer_surname=" + customer_surname + ", customer_tel=" + customer_tel + ", customer_point=" + customer_point + ", customer_amount=" + customer_amount + ", customer_discount=" + customer_discount + '}';
    }
    
    public static Customer fromRS(ResultSet rs){
        Customer cus = new Customer();
        try {
            cus.setCustomer_id(rs.getInt("customer_id"));
            cus.setCustomer_name(rs.getString("customer_name"));
            cus.setCustomer_surname(rs.getString("customer_surname"));
            cus.setCustomer_tel(rs.getString("customer_tel"));
            cus.setCustomer_point(rs.getInt("customer_point"));
            cus.setCustomer_amount(rs.getInt("customer_amount"));
            cus.setCustomer_discount(rs.getInt("customer_discount"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cus;
    }
    
    
}
