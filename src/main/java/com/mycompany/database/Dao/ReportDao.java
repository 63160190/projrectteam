/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Report;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oil
 */
public class ReportDao implements Dao<Report> {

    @Override
    public Report get(int id) {
        Report report = null;
        String sql = "SELECT * FROM Report WHERE report_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                report = Report.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return report;
    }

    @Override
    public List<Report> getAll() {
        ArrayList<Report> list = new ArrayList();
        String sql = "SELECT * FROM Report";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Report report = Report.fromRS(rs);
                list.add(report);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Report save(Report obj) {
        String sql = "INSERT INTO Report (check_stock_id,employee_id,report_want_buy,report_total,)" + "VALUES(?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheck_stock_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getReport_want_buy());
            stmt.setDouble(4, obj.getReport_total());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Report update(Report obj) {
        String sql = "UPDATE Report" + " SET check_stock_id = ?,employee_id = ?,report_want_buy = ?,report_total = ?" + " WHERE report_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCheck_stock_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getReport_want_buy());
            stmt.setDouble(4, obj.getReport_total());
            stmt.setInt(5, obj.getReport_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Report obj) {
        String sql = "DELETE FROM Report WHERE Report_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReport_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Report> getAll(String where, String order) {
        ArrayList<Report> list = new ArrayList();
        String sql = "SELECT * FROM Report where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Report report = Report.fromRS(rs);
                list.add(report);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}

