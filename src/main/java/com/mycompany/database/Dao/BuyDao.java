/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Buy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class BuyDao implements Dao<Buy>{

    @Override
    public Buy get(int id) {
        Buy buy = null;
        String sql = "SELECT * FROM Buy WHERE buy_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                buy = Buy.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return buy;
    }

    @Override
    public List<Buy> getAll() {
         ArrayList<Buy> list = new ArrayList();
        String sql = "SELECT * FROM Buy";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Buy buy = Buy.fromRS(rs);
                list.add(buy);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Buy save(Buy obj) {
        String sql = "INSERT INTO Buy (report_id,employee_id,buy_date" + "VALUES(?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);         
            stmt.setInt(1, obj.getReport_id());
            stmt.setInt(2, obj.getEmpolyee_id());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getBuy_date()));

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Buy update(Buy obj) {
        String sql = "UPDATE Buy" + " SET report_id = ?,employee_id = ?,buy_date = ?" + " WHERE buy_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReport_id());
            stmt.setInt(2, obj.getEmpolyee_id());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getBuy_date()));
            stmt.setInt(4, obj.getBuy_id());
            
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Buy obj) {
        String sql = "DELETE FROM Buy WHERE buy_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBuy_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Buy> getAll(String where, String order) {
        ArrayList<Buy> list = new ArrayList();
        String sql = "SELECT * FROM Buy where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Buy buy = Buy.fromRS(rs);
                list.add(buy);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
