/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Order_item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class Order_itemDao implements Dao<Order_item>{

    @Override
    public Order_item get(int id) {
       Order_item item = null;
        String sql = "SELECT * FROM Order_item WHERE order_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Order_item.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Order_item> getAll() {
       ArrayList<Order_item> list = new ArrayList();
        String sql = "SELECT * FROM Order_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order_item item = Order_item.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Order_item save(Order_item obj) {
        String sql = "INSERT INTO Order_item (product_id,order_item_amount)" + "VALUES(?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct_id());
            stmt.setInt(2, obj.getOrder_item_amount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Order_item update(Order_item obj) {
        String sql = "UPDATE Order_item" + " SET product_id = ?,order_item_amount = ?" + " WHERE order_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct_id());
            stmt.setInt(2, obj.getOrder_item_amount());
            stmt.setInt(3, obj.getOrder_item_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Order_item obj) {
        String sql = "DELETE FROM Order_item WHERE order_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrder_item_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Order_item> getAll(String where, String order) {
        ArrayList<Order_item> list = new ArrayList();
        String sql = "SELECT * FROM Order_item where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order_item item = Order_item.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}