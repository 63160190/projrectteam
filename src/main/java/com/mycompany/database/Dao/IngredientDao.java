package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Ingredient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class IngredientDao implements Dao<Ingredient> {

    @Override
    public Ingredient get(int id) {
        Ingredient ingredient = null;
        String sql = "SELECT * FROM Ingredient WHERE ingredient_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingredient = Ingredient.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ingredient;
    }

    @Override
    public List<Ingredient> getAll() {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM Ingredient";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ingredient save(Ingredient obj) {
        String sql = "INSERT INTO Ingredient (ingredient_name,ingredient_unit,ingredient_total,ingredient_amount_in,ingredient_amount_out,ingredient_min_amount)" + "VALUES(?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getIngredient_name());
            stmt.setString(2, obj.getIngredient_unit());
            stmt.setInt(3, obj.getIngredient_total());
            stmt.setInt(4, obj.getIngredient_amount_in());
            stmt.setInt(5, obj.getIngredient_amount_out());
            stmt.setInt(6, obj.getIngredient_min_amount());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setIngredient_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ingredient update(Ingredient obj) {
        String sql = "UPDATE Ingredient"
                + " SET ingredient_name = ?,ingredient_unit = ?,ingredient_total = ?,ingredient_amount_in = ?,ingredient_amount_out = ?,ingredient_min_amount = ?"
                + " WHERE ingredient_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getIngredient_name());
            stmt.setString(2, obj.getIngredient_unit());
            stmt.setInt(3, obj.getIngredient_total());
            stmt.setInt(4, obj.getIngredient_amount_in());
            stmt.setInt(5, obj.getIngredient_amount_out());
            stmt.setInt(6, obj.getIngredient_min_amount());
            stmt.setInt(7, obj.getIngredient_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ingredient obj) {
        String sql = "DELETE FROM Ingredient WHERE ingredient_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getIngredient_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Ingredient> getAll(String where, String search) {
        ArrayList<Ingredient> list = new ArrayList();
        String sql = "SELECT * FROM Ingredient WHERE " + where + " LIKE '%" + search + "%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ingredient ingredient = Ingredient.fromRS(rs);
                list.add(ingredient);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Ingredient getName(String Name) {
        Ingredient ingre = null;
        String sql = "SELECT * FROM Ingredient WHERE ingredient_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, Name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ingre = Ingredient.fromRS(rs);
            }
            if (ingre == null) {
                return null;
            }
            return ingre;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }
}
