/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        Orders order = null;
        String sql = "SELECT * FROM Orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                order = Orders.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return order;
    }

    @Override
    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM Orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders product = Orders.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {
        String sql = "INSERT INTO Orders (order_item_id,employee_id,customer_id,order_queue,order_total,order_total_net,order_cash,order_change,order_discount,order_datetime,order_qty)" + "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrder_item_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getCustomer_id());
            stmt.setInt(4, obj.getOrder_queue());
            stmt.setDouble(5, obj.getOrder_total());
            stmt.setDouble(6, obj.getOrder_total_net());
            stmt.setDouble(7, obj.getOrder_cash());
            stmt.setDouble(8, obj.getOrder_change());
            stmt.setDouble(9, obj.getOrder_discount());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(10, sdf.format(obj.getOrder_datetime()));
            stmt.setInt(11, obj.getOrder_qty());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Orders update(Orders obj) {
       String sql = "UPDATE Orders" + " SET order_item_id = ?,employee_id = ?,customer_id = ?,order_queue = ?,order_total = ?,order_total_net = ?,order_cash = ?,order_change = ?,order_discount = ?,order_datetime = ?,order_qty = ?" + " WHERE order_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrder_item_id());
            stmt.setInt(2, obj.getEmployee_id());
            stmt.setInt(3, obj.getCustomer_id());
            stmt.setInt(4, obj.getOrder_queue());
            stmt.setDouble(5, obj.getOrder_total());
            stmt.setDouble(6, obj.getOrder_total_net());
            stmt.setDouble(7, obj.getOrder_cash());
            stmt.setDouble(8, obj.getOrder_change());
            stmt.setDouble(9, obj.getOrder_discount());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(10, sdf.format(obj.getOrder_datetime()));
            stmt.setInt(11, obj.getOrder_qty());
            stmt.setInt(12, obj.getOrder_id());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Orders obj) {
         String sql = "DELETE FROM Orders WHERE order_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrder_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Orders> getAll(String where, String order) {
       ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM Orders where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders orders = Orders.fromRS(rs);
                list.add(orders);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
