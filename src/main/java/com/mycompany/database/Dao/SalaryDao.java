/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.Dao;

import com.mycompany.database.Dao.Dao;
import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oil
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM Salary WHERE salary_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salary = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Salary save(Salary obj) {
        String sql = "INSERT INTO Salary (work_time_id,salary_start_pay,salary_end_pay,salary_decrease,salary_sum)" + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWork_time_id());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(2, sdf.format(obj.getSalary_start_pay()));
            stmt.setString(3, sdf.format(obj.getSalary_end_pay()));
            stmt.setDouble(4, obj.getSalary_decrease());
            stmt.setDouble(5, obj.getSalaly_sum());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Salary update(Salary obj) {
        String sql = "UPDATE Report" + " SET work_time_id = ?,salary_start_pay = ?,salary_end_pay = ?,salary_decrease = ?,salary_sum = ?" + " WHERE salary_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWork_time_id());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(2, sdf.format(obj.getSalary_start_pay()));
            stmt.setString(3, sdf.format(obj.getSalary_end_pay()));
            stmt.setDouble(4, obj.getSalary_decrease());
            stmt.setDouble(5, obj.getSalaly_sum());
            stmt.setInt(6, obj.getSalary_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Salary obj) {
        String sql = "DELETE FROM Salary WHERE Salary_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalary_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
