/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Work_Time;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oil
 */
public class Work_TimeDao implements Dao<Work_Time> {

    @Override
    public Work_Time get(int id) {
        Work_Time work_Time = null;
        String sql = "SELECT * FROM Work_Time WHERE work_time_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                work_Time = Work_Time.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return work_Time;
    }

    @Override
    public List<Work_Time> getAll() {
        ArrayList<Work_Time> list = new ArrayList();
        String sql = "SELECT * FROM Work_Time";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Work_Time work_Time = Work_Time.fromRS(rs);
                list.add(work_Time);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Work_Time save(Work_Time obj) {
        String sql = "INSERT INTO Work_Time (work_time_start_time,work_time_end_time,work_time_sum_worktime)" + "VALUES(?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(1, sdf.format(obj.getWork_time_start_time()));
            stmt.setString(2, sdf.format(obj.getWork_time_end_time()));
            stmt.setString(2, sdf.format(obj.getWork_time_sum_worktime()));

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Work_Time update(Work_Time obj) {
        String sql = "UPDATE Work_Time" + " SET work_time_start_time = ?,work_time_end_time = ?,work_time_sum_worktime = ?" + " WHERE work_time_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(1, sdf.format(obj.getWork_time_start_time()));
            stmt.setString(1, sdf.format(obj.getWork_time_end_time()));
            stmt.setString(1, sdf.format(obj.getWork_time_sum_worktime()));
            stmt.setInt(8, obj.getWork_time_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public Work_Time update1(Work_Time obj) {
        String sql = "UPDATE Work_Time SET work_time_start_time = DATETIME('now', 'localtime') WHERE work_time_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWork_time_id());
            stmt.executeUpdate();
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }
    
    public Work_Time update2(Work_Time obj) {
        String sql = "UPDATE Work_Time SET work_time_end_time = DATETIME('now', 'localtime') WHERE work_time_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWork_time_id());
            stmt.executeUpdate();
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Work_Time obj) {
        String sql = "DELETE FROM Work_Time WHERE Work_time_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWork_time_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Work_Time> getAll(String where, String order) {
        ArrayList<Work_Time> list = new ArrayList();
        String sql = "SELECT * FROM Work_Time where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Work_Time work_Time = Work_Time.fromRS(rs);
                list.add(work_Time);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
