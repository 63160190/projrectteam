package com.mycompany.database.Dao;

import com.mycompany.databasehelper.DatabaseHelper;
import com.mycompany.model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao implements Dao<Customer> {

    @Override
    public Customer get(int id) {
        Customer customer= null;
        String sql = "SELECT * FROM Customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM Customer";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Customer save(Customer obj) {
        String sql = "INSERT INTO Customer (customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type)" + "VALUES(?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCustomer_name());
            stmt.setString(2, obj.getCustomer_surname());
            stmt.setString(3, obj.getCustomer_tel());
            stmt.setInt(4, obj.getCustomer_point());
            stmt.setInt(5, obj.getCustomer_amount());
            stmt.setInt(6, obj.getCustomer_discount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setCustomer_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Customer update(Customer obj) {
        String sql = "UPDATE Customer" + " SET customer_name = ?,customer_surname = ?,customer_tel = ?,customer_point = ?,customer_amount = ?,customer_discount = ?,customer_type = ?" + " WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCustomer_name());
            stmt.setString(2, obj.getCustomer_surname());
            stmt.setString(3, obj.getCustomer_tel());
            stmt.setInt(4, obj.getCustomer_point());
            stmt.setInt(5, obj.getCustomer_amount());
            stmt.setInt(6, obj.getCustomer_discount());
            stmt.setInt(8, obj.getCustomer_id());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Customer obj) {
        String sql = "DELETE FROM Customer WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCustomer_id());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Customer> getAll(String where, String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM Customer where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Customer getTel(String phone) {
        Customer customer = null;
        String sql = "SELECT * FROM Customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, phone);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                customer = Customer.fromRS(rs);
            }
            else {
                return null;
            }
            return customer;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

}