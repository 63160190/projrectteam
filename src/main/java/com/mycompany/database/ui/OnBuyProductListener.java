/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.ui;

import com.mycompany.model.Product;

/**
 *
 * @author BankMMT
 */
public interface OnBuyProductListener {
    public void buy (Product product, int amount);
}
