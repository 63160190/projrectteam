/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.database.ui;

import com.mycompany.database.Dao.CustomerDao;
import com.mycompany.model.Customer;
import com.mycompany.service.CustomerService;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sairung
 */
public class CustomerPanel extends javax.swing.JPanel {

    private final CustomerService customerService;
    private Customer editedCustomer;
    private CustomerDao customerDao;
    private List<Customer> list;

    /**
     * Creates new form Store
     */
    public CustomerPanel() {
        initComponents();

        customerService = new CustomerService();
        customerDao = new CustomerDao();
        list = customerService.getCustomer();

        tblCustomer.setModel(new AbstractTableModel() {
            String[] columnName = {"ID", "Name", "Surname", "Tel", "Point", "Amount", "Discount"};

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return customerDao.getAll().size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Customer customer = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return customer.getCustomer_id();
                    case 1:

                        return customer.getCustomer_name();

                    case 2:

                        return customer.getCustomer_surname();

                    case 3:

                        return customer.getCustomer_tel();

                    case 4:

                        return customer.getCustomer_point();

                    case 5:

                        return customer.getCustomer_amount();

                    case 6:

                        return customer.getCustomer_discount();

                    default:
                        return null;
                }
            }
        });
        enableForm(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblCustomer = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        tfSurname = new javax.swing.JTextField();
        tfTel = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        tfName = new javax.swing.JTextField();
        txtName = new javax.swing.JLabel();
        txtSurName = new javax.swing.JLabel();
        txtTel = new javax.swing.JLabel();
        tfPoint = new javax.swing.JTextField();
        tfDiscount = new javax.swing.JTextField();
        tfAmount = new javax.swing.JTextField();
        txtPoint = new javax.swing.JLabel();
        txtAmount = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btndel = new javax.swing.JButton();

        tblCustomer.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCustomer.setRowHeight(25);
        jScrollPane1.setViewportView(tblCustomer);

        jPanel1.setBackground(new java.awt.Color(242, 255, 234));

        tfSurname.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        tfTel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btnSave.setBackground(new java.awt.Color(0, 153, 0));
        btnSave.setForeground(new java.awt.Color(242, 255, 234));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(204, 0, 0));
        btnCancel.setForeground(new java.awt.Color(255, 204, 204));
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        tfName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txtName.setText("Name :");

        txtSurName.setText("Surname :");

        txtTel.setText("Tel :");

        txtPoint.setText("Point :");

        txtAmount.setText("Amount :");

        txtDiscount.setText("Discount :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAmount, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtPoint)
                            .addComponent(txtName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfAmount)
                            .addComponent(tfName, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(tfPoint))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtSurName)
                            .addComponent(txtDiscount)
                            .addComponent(txtTel, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfTel, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(tfSurname, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(tfDiscount))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName)
                    .addComponent(txtSurName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTel)
                    .addComponent(tfPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPoint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAmount)
                    .addComponent(txtDiscount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnSave))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(242, 255, 234));

        btnAdd.setBackground(new java.awt.Color(0, 153, 0));
        btnAdd.setForeground(new java.awt.Color(242, 255, 234));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btndel.setBackground(new java.awt.Color(204, 0, 0));
        btndel.setForeground(new java.awt.Color(255, 204, 204));
        btndel.setText("Del");
        btndel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btndel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btndel)
                    .addComponent(btnEdit)
                    .addComponent(btnAdd))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (editedCustomer.getCustomer_id() < 0) {
            setFormToObject();
            enableForm(false);
            customerService.addNew(editedCustomer);
            refreshTable();
        } 
        else {
            setFormToObject();
            enableForm(false);
            customerService.update(editedCustomer);
            refreshTable();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedCustomer = new Customer();
        setObjectToForm();
        enableForm(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectIndex = tblCustomer.getSelectedRow();
        if (selectIndex >= 0) {
            editedCustomer = list.get(selectIndex);
            setObjectToForm();
            enableForm(true);
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btndelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndelActionPerformed
        int selectedIndex = tblCustomer.getSelectedRow();
        if (selectedIndex >= 0) {
            editedCustomer = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

            if (input == 0) {
                customerService.delete(editedCustomer);
                refreshTable();
            }
            enableForm(false);
        }
    }//GEN-LAST:event_btndelActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearTextFrom();
        enableForm(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void setFormToObject() {
        editedCustomer.setCustomer_name(tfName.getText());
        editedCustomer.setCustomer_surname(tfSurname.getText());
        editedCustomer.setCustomer_tel(tfTel.getText());
        editedCustomer.setCustomer_point(Integer.valueOf(tfPoint.getText()));
        editedCustomer.setCustomer_amount(Integer.valueOf(tfAmount.getText()));
        editedCustomer.setCustomer_discount(Integer.valueOf(tfDiscount.getText()));
    }

    private void setObjectToForm() {
        if (editedCustomer.getCustomer_point() == 0 && (editedCustomer.getCustomer_amount() == 0 )) {
            tfName.setText(editedCustomer.getCustomer_name());
            tfSurname.setText(editedCustomer.getCustomer_surname());
            tfTel.setText(editedCustomer.getCustomer_tel());
            tfPoint.setText("");
            tfAmount.setText("");
            tfDiscount.setText("");
        } else {
            tfName.setText(editedCustomer.getCustomer_name());
            tfSurname.setText(editedCustomer.getCustomer_surname());
            tfTel.setText(editedCustomer.getCustomer_tel());
            tfPoint.setText("" + editedCustomer.getCustomer_point());
            tfAmount.setText("" + editedCustomer.getCustomer_amount());
            tfDiscount.setText("" + editedCustomer.getCustomer_discount());
        }

    }

    private void refreshTable() {
        list = customerService.getCustomer();
        tblCustomer.revalidate();
        tblCustomer.repaint();
    }

    private void enableForm(boolean status) {
        if (!status) {
            clearTextFrom();
        }
        tfName.setEnabled(status);
        tfSurname.setEnabled(status);
        tfTel.setEnabled(status);
        tfPoint.setEnabled(status);
        tfAmount.setEnabled(status);
        tfDiscount.setEnabled(status);
        btnSave.setEnabled(status);
        btnCancel.setEnabled(status);
    }

    private void clearTextFrom() {
        tfName.setText("");
        tfSurname.setText("");
        tfTel.setText("");
        tfPoint.setText("");
        tfAmount.setText("");
        tfDiscount.setText("");
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btndel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblCustomer;
    private javax.swing.JTextField tfAmount;
    private javax.swing.JTextField tfDiscount;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfPoint;
    private javax.swing.JTextField tfSurname;
    private javax.swing.JTextField tfTel;
    private javax.swing.JLabel txtAmount;
    private javax.swing.JLabel txtDiscount;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtPoint;
    private javax.swing.JLabel txtSurName;
    private javax.swing.JLabel txtTel;
    // End of variables declaration//GEN-END:variables
}
