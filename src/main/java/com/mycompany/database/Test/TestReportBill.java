/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Test;

import com.mycompany.model.ReportBill;
import com.mycompany.service.ReportBillSevice;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestReportBill {

    public static void main(String[] args) {
        ReportBillSevice reportService = new ReportBillSevice();

        List<ReportBill> report = reportService.getReportBill();
        for (ReportBill r : report) {
            System.out.println(r);
        }
    }
}
