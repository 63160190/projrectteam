/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Test;

import com.mycompany.model.ReportIngredient;
import com.mycompany.service.ReportIngredientService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestReportIngredient {

    public static void main(String[] args) {
        ReportIngredientService reportService = new ReportIngredientService();

        List<ReportIngredient> report = reportService.getReportIngredient();
        for (ReportIngredient r : report) {
            System.out.println(r);
        }
    }
}
