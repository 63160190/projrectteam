/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.database.Test;

import com.mycompany.model.ReportSale;
import com.mycompany.service.ReportSaleService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportSaleService reportService = new ReportSaleService();

        List<ReportSale> report = reportService.getReportSaleByDay();
        for (ReportSale r : report) {
            System.out.println(r);
        }
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth();
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }

        List<ReportSale> reportYear = reportService.getReportSaleByYear();
        for (ReportSale r : reportYear) {
            System.out.println(r);
        }
    }
}
