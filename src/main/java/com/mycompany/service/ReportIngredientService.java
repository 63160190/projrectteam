/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.service;

import com.mycompany.database.Dao.ReportIngredientDao;
import com.mycompany.model.ReportIngredient;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReportIngredientService {
     public List<ReportIngredient> getReportIngredient(){
        ReportIngredientDao dao = new ReportIngredientDao();
        return dao.getReport();
    }
}
