/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.service;

import com.mycompany.database.Dao.Work_TimeDao;
import com.mycompany.model.Work_Time;

/**
 *
 * @author sairung
 */
public class Work_TimeService {

    public Work_Time updatein(Work_Time editedWorkTime) {
        Work_TimeDao worktimeDao = new Work_TimeDao();
        return worktimeDao.update1(editedWorkTime);
    }
    public Work_Time updateout(Work_Time editedWorkTime) {
        Work_TimeDao worktimeDao = new Work_TimeDao();
        return worktimeDao.update2(editedWorkTime);
    }
}
