/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.service;

import com.mycompany.database.Dao.EmployeeDao;
import com.mycompany.model.Employee;
import java.util.List;

/**
 *
 * @author sairung
 */
public class EmployeeService {
    
    public List<Employee> getEmployee(){
      EmployeeDao employeeDao = new EmployeeDao();
      return employeeDao.getAll();
  }
    
    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }
    
    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
    
    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }
}
