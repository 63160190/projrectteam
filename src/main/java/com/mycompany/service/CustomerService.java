/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.service;

import com.mycompany.database.Dao.CustomerDao;
import com.mycompany.model.Customer;
import java.util.List;

/**
 *
 * @author sairung
 */
public class CustomerService {
     
    public List<Customer> getCustomer(){
      CustomerDao customerDao = new CustomerDao();
      return customerDao.getAll();
  }
    
    public Customer addNew(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.save(editedCustomer);
    }
    
    public int delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCustomer);
    }
    
    public Customer update(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCustomer);
    }
}
