/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.service;

import com.mycompany.database.Dao.ReportBillDao;
import com.mycompany.model.ReportBill;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReportBillSevice {
    public List<ReportBill> getReportBill(){
        ReportBillDao dao = new  ReportBillDao();
        return dao.getReport();
    }
}
