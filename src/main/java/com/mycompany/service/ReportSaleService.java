/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.service;

import com.mycompany.database.Dao.ReportSaleDao;
import com.mycompany.model.ReportSale;
import java.util.List;

/**
 *
 * @author acer
 */
public class ReportSaleService {
    public List<ReportSale> getReportSaleByDay(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getDayReport();
    }
    public List<ReportSale> getReportSaleByMonth(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }
    
    public List<ReportSale> getReportSaleByYear(){
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYearReport();
    }
    
}
