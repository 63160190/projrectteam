/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.service;

import com.mycompany.database.Dao.IngredientDao;
import com.mycompany.model.Ingredient;
import java.util.List;

/**
 *
 * @author sairung
 */
public class IngredientService {
    
    public List<Ingredient> getIngredient(){
      IngredientDao ingredientDao = new IngredientDao();
      return ingredientDao.getAll();
  }
    
    public Ingredient addNew(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.save(editedIngredient);
    }
    
    public int delete(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.delete(editedIngredient);
    }
    
    public Ingredient update(Ingredient editedIngredient) {
        IngredientDao ingredientDao = new IngredientDao();
        return ingredientDao.update(editedIngredient);
    }
}
